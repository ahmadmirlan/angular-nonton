import {Component, OnDestroy, OnInit} from '@angular/core';
import {select, Store} from '@ngrx/store';
import {AppState} from '../../../../core/store/reducers';
import {Movie} from '../../../../core/movie/model/movie.model';
import {LoadAllPublishedMovieRequested} from '../../../../core/movie/action/movie.actions';
import {QueryParamsModel} from '../../../../core/shared/models/queryParams.model';
import {selectAllPublishedMovie} from '../../../../core/movie/selector/movie.selector';
import {Subscription} from 'rxjs';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit, OnDestroy {

  publishedMovies: Movie[] = [];
  subcriptions: Subscription[] = [];

  constructor(private store: Store<AppState>) {
  }

  ngOnInit(): void {
    // Load movies data
    const movieSubs = this.store.pipe(select(selectAllPublishedMovie)).subscribe(resp => {
      if (resp) {
        this.publishedMovies = resp;
      } else {
        this.store.dispatch(new LoadAllPublishedMovieRequested({queryParams: this.queryPublishedMovie()}));
      }
    });
    this.subcriptions.push(movieSubs);
  }

  // Build params
  queryPublishedMovie(): QueryParamsModel {
    return new QueryParamsModel({});
  }

  ngOnDestroy(): void {
    // Unsubscribe observable
    this.subcriptions.forEach(sub => sub.unsubscribe());
  }
}
