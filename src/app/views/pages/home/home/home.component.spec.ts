import {async, ComponentFixture, TestBed} from '@angular/core/testing';

import {HomeComponent} from './home.component';
import {MockStore, provideMockStore} from '@ngrx/store/testing';
import {AppState} from '../../../../core/store/reducers';
import {MemoizedSelector, Store} from '@ngrx/store';
import {selectAllPublishedMovie} from '../../../../core/movie/selector/movie.selector';
import {MovieState} from '../../../../core/movie/reducer/movie.reducer';
import {Movie} from '../../../../core/movie/model/movie.model';
import {By} from '@angular/platform-browser';
import {MatIconModule} from '@angular/material/icon';
import {MatButtonModule} from '@angular/material/button';
import {RouterTestingModule} from '@angular/router/testing';

describe('HomeComponent', () => {
  let component: HomeComponent;
  let fixture: ComponentFixture<HomeComponent>;
  let mockStore: MockStore<AppState>;
  let selectAllPublishedMovieMock: MemoizedSelector<MovieState, Movie[]>;
  let movies: Movie[];
  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [HomeComponent],
      imports: [
        MatIconModule,
        MatButtonModule,
        RouterTestingModule
      ],
      providers: [
        provideMockStore(),
      ]
    })
      .compileComponents();
    const movie1 = new Movie();
    movie1.clear();
    movie1.id = '1';
    movie1.title = 'Joker';

    const movie2 = new Movie();
    movie2.clear();
    movie2.id = '2';
    movie2.title = 'It';

    const movie3 = new Movie();
    movie3.clear();
    movie3.id = '3';
    movie3.title = 'Angling Darma';

    movies = [movie1, movie2, movie3];
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(HomeComponent);
    component = fixture.componentInstance;
    mockStore = TestBed.get(Store);
    selectAllPublishedMovieMock = mockStore.overrideSelector(selectAllPublishedMovie, movies);
    fixture.detectChanges();
  });

  it('should init movies data from selectAllPublishedMovie', () => {
    selectAllPublishedMovieMock.setResult(movies);
    mockStore.refreshState();
    fixture.detectChanges();

    expect(component.publishedMovies).toEqual(movies);
  });

  it('should render all movies as a card', () => {
    selectAllPublishedMovieMock.setResult(movies);
    mockStore.refreshState();
    fixture.detectChanges();

    const card = fixture.debugElement.queryAll(By.css('.my-card'));
    expect(card.length).toEqual(3);
  });
});
