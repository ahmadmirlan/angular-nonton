import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {HomeComponent} from './home/home.component';
import {RouterModule, Routes} from '@angular/router';
import {MatIconModule} from '@angular/material/icon';
import {MatButtonModule} from '@angular/material/button';
import {StoreModule} from '@ngrx/store';
import {movieReducer} from '../../../core/movie/reducer/movie.reducer';
import {EffectsModule} from '@ngrx/effects';
import {MovieEffects} from '../../../core/movie/effect/movie.effects';
import {categoryReducer} from '../../../core/category/reducer/category.reducer';
import {CategoryEffects} from '../../../core/category/effect/category.effects';

const ROUTES: Routes = [
  {
    path: '',
    component: HomeComponent
  }
];

@NgModule({
  declarations: [HomeComponent],
  imports: [
    CommonModule,
    RouterModule.forChild(ROUTES),
    MatIconModule,
    MatButtonModule,
    StoreModule.forFeature('movie', movieReducer),
    StoreModule.forFeature('category', categoryReducer),
    EffectsModule.forFeature([MovieEffects, CategoryEffects])
  ]
})

export class HomeModule {
}
