import {Component, OnDestroy, OnInit} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {Router} from '@angular/router';
import {AuthService} from '../../../../core/auth/service/auth.service';
import {PasswordValidator} from './password.validator';
import {Subscription} from 'rxjs';
import {MatSnackBar} from '@angular/material/snack-bar';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.scss']
})
export class RegisterComponent implements OnInit, OnDestroy {

  subscriptions: Subscription[] = [];
  registerForm: FormGroup;
  duplicateValidation = {
    isEmailExist: false,
    isUsernameExist: false
  };

  constructor(private fb: FormBuilder, private router: Router, private authService: AuthService, private snackBar: MatSnackBar) {
  }

  ngOnInit(): void {
    this.registerForm = this.fb.group({
      firstName: ['', [Validators.required]],
      lastName: ['', [Validators.required]],
      username: ['', [Validators.required, Validators.minLength(6)]],
      email: ['', [Validators.required, Validators.email]],
      password: ['', [Validators.required, Validators.minLength(5)]],
      confirmPassword: ['', [Validators.required]],
      agree: [false, Validators.requiredTrue]
    }, {
      validators: PasswordValidator.MatchPassword
    });
  }

  /*On register submit*/
  onRegister(): void {
    const {firstName, lastName, username, email, password} = this.registerForm.getRawValue();
    const subRegister = this.authService.register({firstName, lastName, username, email, password}).subscribe(resp => {
      if (resp) {
        this.router.navigate(['/auth/login']);
      }
    }, error => {
      this.registerForm.markAllAsTouched();
      this.registerForm.markAsDirty();
      this.snackBar.open('Some thing went error', 'Dismiss', {duration: 5000});
    });
    this.subscriptions.push(subRegister);
  }

  /*
  * Check email exist or nor
  * */
  checkEmail(): void {
    const {email} = this.registerForm.getRawValue();
    if (this.registerForm.controls.email.valid) {
      const emailSub = this.authService.checkExistingEmail(email).subscribe(resp => {
        this.duplicateValidation.isEmailExist = resp;
        if (resp) {
          this.registerForm.controls.email.setErrors(Validators.required);
        }
      });
      this.subscriptions.push(emailSub);
    }
  }

  /*
  * Check username exist or not
  * */
  checkUsername(): void {
    const {username} = this.registerForm.getRawValue();
    if (this.registerForm.controls.username.valid) {
      const usernameSub = this.authService.checkUsername(username).subscribe(resp => {
        this.duplicateValidation.isUsernameExist = resp;
        if (resp) {
          this.registerForm.controls.username.setErrors(Validators.required);
        }
      });
      this.subscriptions.push(usernameSub);
    }
  }

  ngOnDestroy(): void {
    this.subscriptions.forEach(sub => {
      sub.unsubscribe();
    });
  }
}
