import {async, ComponentFixture, fakeAsync, TestBed} from '@angular/core/testing';

import {RegisterComponent} from './register.component';
import {ReactiveFormsModule} from '@angular/forms';
import {Router} from '@angular/router';
import {AuthService} from '../../../../core/auth/service/auth.service';
import {MatFormFieldModule} from '@angular/material/form-field';
import {MatButtonModule} from '@angular/material/button';
import {MatSnackBar, MatSnackBarModule} from '@angular/material/snack-bar';
import {Directive, Input} from '@angular/core';
import {MatInputModule} from '@angular/material/input';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {MatCheckboxModule} from '@angular/material/checkbox';
import {of, throwError} from 'rxjs';
import {By} from '@angular/platform-browser';
import createSpyObj = jasmine.createSpyObj;

class RouterStub {
  navigate(params) {
  }
}

@Directive({
  // tslint:disable-next-line:directive-selector
  selector: '[routerLink]',
  // tslint:disable-next-line:no-host-metadata-property
  host: {'(click)': 'onClick()'}
})
// tslint:disable-next-line:directive-class-suffix
export class RouterLinkDirectiveStub {
  @Input('routerLink') linkParams: any;
  navigatedTo: any = null;

  onClick() {
    this.navigatedTo = this.linkParams;
  }
}

describe('RegisterComponent', () => {
  let component: RegisterComponent;
  let fixture: ComponentFixture<RegisterComponent>;
  let authService;
  let router: Router;
  let snackBar;

  beforeEach(async(() => {
    authService = createSpyObj(['register', 'checkExistingEmail', 'checkUsername']);
    snackBar = createSpyObj(['open']);
    TestBed.configureTestingModule({
      declarations: [RegisterComponent, RouterLinkDirectiveStub],
      imports: [
        ReactiveFormsModule, MatFormFieldModule, MatButtonModule, MatSnackBarModule, MatInputModule,
        BrowserAnimationsModule, MatCheckboxModule
      ],
      providers: [
        {provide: AuthService, useValue: authService},
        {provide: Router, useClass: RouterStub},
        {provide: MatSnackBar, useValue: snackBar},
      ]
    })
      .compileComponents();

    router = TestBed.get(Router);
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RegisterComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  describe('Register Form', () => {
    it('should register form to be invalid (pass does not match)', () => {
      component.registerForm.controls.firstName.setValue('mikel');
      component.registerForm.controls.lastName.setValue('owen');
      component.registerForm.controls.username.setValue('mikelowen');
      component.registerForm.controls.email.setValue('mikelowen@yopmail.com');
      component.registerForm.controls.password.setValue('12345678');
      component.registerForm.controls.confirmPassword.setValue('123456');
      component.registerForm.controls.agree.setValue(false);

      fixture.detectChanges();
      expect(component.registerForm.valid).toBeFalsy();
    });

    it('should register form to be valid', () => {
      component.registerForm.controls.firstName.setValue('mikel');
      component.registerForm.controls.lastName.setValue('owen');
      component.registerForm.controls.username.setValue('mikelowen');
      component.registerForm.controls.email.setValue('mikelowen@yopmail.com');
      component.registerForm.controls.password.setValue('12345678');
      component.registerForm.controls.confirmPassword.setValue('12345678');
      component.registerForm.controls.agree.setValue(true);

      fixture.detectChanges();
      expect(component.registerForm.valid).toBeTruthy();
    });
  });

  it('should navigate to login page after register success', () => {
    authService.register.and.returnValue(of({message: 'Register success'}));
    const spyRouter = spyOn(router, 'navigate');
    component.registerForm.controls.firstName.setValue('mikel');
    component.registerForm.controls.lastName.setValue('owen');
    component.registerForm.controls.username.setValue('mikelowen');
    component.registerForm.controls.email.setValue('mikelowen@yopmail.com');
    component.registerForm.controls.password.setValue('12345678');
    component.registerForm.controls.confirmPassword.setValue('12345678');
    component.registerForm.controls.agree.setValue(true);

    const registerButton = fixture.debugElement.query(By.css('#registerButton'));
    registerButton.triggerEventHandler('click', null);
    fixture.detectChanges();
    const {firstName, lastName, username, email, password} = component.registerForm.getRawValue();
    expect(authService.register).toHaveBeenCalledWith({firstName, lastName, username, email, password});
    expect(spyRouter).toHaveBeenCalled();
  });

  it('should stay on register page when register failed after submit', () => {
    authService.register.and.returnValue(throwError('Invalid username or password'));
    const spyRouter = spyOn(router, 'navigate');
    component.registerForm.controls.firstName.setValue('mikel');
    component.registerForm.controls.lastName.setValue('owen');
    component.registerForm.controls.username.setValue('mikelowen');
    component.registerForm.controls.email.setValue('mikelowen@yopmail.com');
    component.registerForm.controls.password.setValue('12345678');
    component.registerForm.controls.confirmPassword.setValue('12345678');
    component.registerForm.controls.agree.setValue(true);

    const registerButton = fixture.debugElement.query(By.css('#registerButton'));
    registerButton.triggerEventHandler('click', null);
    fixture.detectChanges();
    const {firstName, lastName, username, email, password} = component.registerForm.getRawValue();
    expect(authService.register).toHaveBeenCalledWith({firstName, lastName, username, email, password});
    expect(spyRouter).not.toHaveBeenCalled();
    expect(snackBar.open).toHaveBeenCalled();
  });

  it('should check existing email or not', () => {
    component.registerForm.controls.firstName.setValue('mikel');
    component.registerForm.controls.lastName.setValue('owen');
    component.registerForm.controls.username.setValue('mikelowen');
    component.registerForm.controls.email.setValue('mikelowen@yopmail.com');
    component.registerForm.controls.password.setValue('12345678');
    component.registerForm.controls.confirmPassword.setValue('12345678');
    component.registerForm.controls.agree.setValue(true);

    authService.checkExistingEmail.and.returnValue(of(true));
    const inputEmail = fixture.debugElement.query(By.css('#email'));
    inputEmail.triggerEventHandler('focusout', null);
    fixture.detectChanges();

    expect(authService.checkExistingEmail).toHaveBeenCalledWith(component.registerForm.controls.email.value);
    expect(component.duplicateValidation.isEmailExist).toEqual(true);
  });

  it('should check existing username or not', fakeAsync(() => {
    component.registerForm.controls.firstName.setValue('mikel');
    component.registerForm.controls.lastName.setValue('owen');
    component.registerForm.controls.username.setValue('mikelowen');
    component.registerForm.controls.email.setValue('mikelowen@yopmail.com');
    component.registerForm.controls.password.setValue('12345678');
    component.registerForm.controls.confirmPassword.setValue('12345678');
    component.registerForm.controls.agree.setValue(true);

    authService.checkUsername.and.returnValue(of(true));
    const inputUsername = fixture.debugElement.query(By.css('#username'));
    inputUsername.triggerEventHandler('focusout', null);
    fixture.detectChanges();

    expect(authService.checkUsername).toHaveBeenCalledWith(component.registerForm.controls.username.value);
    expect(component.duplicateValidation.isUsernameExist).toEqual(true);
  }));
});
