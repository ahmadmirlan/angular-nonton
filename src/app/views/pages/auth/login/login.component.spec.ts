import {async, ComponentFixture, TestBed} from '@angular/core/testing';

import {LoginComponent} from './login.component';
import {MatFormFieldModule} from '@angular/material/form-field';
import {MatInputModule} from '@angular/material/input';
import {MatCheckboxModule} from '@angular/material/checkbox';
import {MatButtonModule} from '@angular/material/button';
import {ReactiveFormsModule} from '@angular/forms';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {Router} from '@angular/router';
import {of, throwError} from 'rxjs';
import {User} from '../../../../core/user/model/user';
import {HttpClientTestingModule} from '@angular/common/http/testing';
import {By} from '@angular/platform-browser';
import {AuthService} from '../../../../core/auth/service/auth.service';
import {MatSnackBar, MatSnackBarModule} from '@angular/material/snack-bar';
import {Directive, Input} from '@angular/core';

class RouterStub {
  navigate(params) {
  }
}

@Directive({
  // tslint:disable-next-line:directive-selector
  selector: '[routerLink]',
  // tslint:disable-next-line:no-host-metadata-property
  host: {'(click)': 'onClick()'}
})
// tslint:disable-next-line:directive-class-suffix
export class RouterLinkDirectiveStub {
  @Input('routerLink') linkParams: any;
  navigatedTo: any = null;

  onClick() {
    this.navigatedTo = this.linkParams;
  }
}


describe('LoginComponent', () => {
  let component: LoginComponent;
  let fixture: ComponentFixture<LoginComponent>;
  let router: RouterStub;
  let mockAuthService;
  let mockSnackBar;

  beforeEach(async(() => {
    mockAuthService = jasmine.createSpyObj(['login']);
    mockSnackBar = jasmine.createSpyObj(['open']);
    TestBed.configureTestingModule({
      declarations: [LoginComponent, RouterLinkDirectiveStub],
      imports: [
        HttpClientTestingModule, MatFormFieldModule, MatInputModule, MatCheckboxModule,
        MatButtonModule, ReactiveFormsModule, BrowserAnimationsModule, MatSnackBarModule
      ],
      providers: [{provide: Router, useClass: RouterStub}, {provide: AuthService, useValue: mockAuthService},
        {provide: MatSnackBar, useValue: mockSnackBar}]
    })
      .compileComponents();
    router = TestBed.get(Router);
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LoginComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  describe('Login Form', () => {
    it('should loginForm invalid', () => {
      component.loginForm.controls.username.setValue('2');
      component.loginForm.controls.password.setValue('1');
      fixture.detectChanges();
      expect(component.loginForm.valid).toBeFalsy();
    });

    it('should loginForm valid', () => {
      component.loginForm.controls.username.setValue('johndoe');
      component.loginForm.controls.password.setValue('123456');
      fixture.detectChanges();
      expect(component.loginForm.valid).toBeTruthy();
    });
  });

  describe('Submit onLogin function', () => {
    it('should call api login', () => {
      const user = new User();
      user.clear();
      mockAuthService.login.and.returnValue(of(user));
      component.onLoginSubmit();

      expect(mockAuthService.login).toHaveBeenCalled();
    });
  });

/*  it('should navigate to register page', () => {
    const spyRouter = spyOn(router, 'navigate');
    component.onNavigateToRegisterPage();
    expect(spyRouter).toHaveBeenCalled();
  });*/

  it('should navigate to home after login success', () => {
    const user = new User();
    user.clear();
    user.firstName = 'John';
    user.lastName = 'Doe';
    user.username = 'johndoe';
    user.username = 'johndoe@yopmail.com';

    component.loginForm.controls.username.setValue('johndoe');
    component.loginForm.controls.password.setValue('123456');

    mockAuthService.login.and.returnValue(of(user));
    const spyRouter = spyOn(router, 'navigate');
    const loginButton = fixture.debugElement.query(By.css('#loginButton'));
    loginButton.triggerEventHandler('click', null);
    fixture.detectChanges();

    expect(spyRouter).toHaveBeenCalledWith(['home']);
  });

  it('should not call navigate route and show error message when login fail', () => {
    const spyRouter = spyOn(router, 'navigate');
    mockSnackBar.open.and.returnValue('Invalid username or password');
    mockAuthService.login.and.returnValue(throwError('Invalid email or password'));
    const loginButton = fixture.debugElement.query(By.css('#loginButton'));
    loginButton.triggerEventHandler('click', null);
    fixture.detectChanges();

    expect(mockSnackBar.open).toHaveBeenCalled();
    expect(spyRouter).not.toHaveBeenCalled();
  });
});
