import {Component, OnInit} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {Router} from '@angular/router';
import {AuthService} from '../../../../core/auth/service/auth.service';
import {MatSnackBar} from '@angular/material/snack-bar';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {

  // Declare login form
  loginForm: FormGroup;
  isLoading = false;
  constructor(private fb: FormBuilder, private router: Router, private authService: AuthService, private snackBar: MatSnackBar) {
  }

  ngOnInit(): void {
    this.loginForm = this.fb.group({
      username: ['', [Validators.required, Validators.minLength(5)]],
      password: ['', [Validators.required, Validators.minLength(4)]]
    });
  }

  // Submit login form
  onLoginSubmit(): void {
    const {username, password} = this.loginForm.getRawValue();
    this.isLoading = true;
    this.authService.login(username, password).subscribe((resp) => {
      this.router.navigate(['home']);
      this.isLoading = false;
    }, error => {
      console.log('invalid username or password');
      this.isLoading = false;
      this.snackBar.open('Invalid username or password', 'Dismiss', {duration: 3000});
    });
  }
}
