import {Component, OnInit} from '@angular/core';
import {MatDialogRef} from '@angular/material/dialog';
import {Category} from '../../../../core/category/model/category.model';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {Store} from '@ngrx/store';
import {AppState} from '../../../../core/store/reducers';
import {CreateCategoryRequested} from '../../../../core/category/action/category.actions';

@Component({
  selector: 'app-category-dialog',
  templateUrl: './category-dialog.component.html',
  styleUrls: ['./category-dialog.component.scss']
})
export class CategoryDialogComponent implements OnInit {

  category: Category;
  categoryForm: FormGroup;

  constructor(private matRefDialog: MatDialogRef<CategoryDialogComponent>, private fb: FormBuilder, private store: Store<AppState>) {
  }

  ngOnInit(): void {
    this.categoryForm = this.fb.group({
      name: [this.category.name ? this.category.name : '', [Validators.required, Validators.minLength(2)]]
    });
  }

  onDialogClose(): void {
    this.matRefDialog.close();
  }

  onSaveCategory(): void {
    const newCategory = new Category();
    newCategory.clear();
    newCategory.name = this.categoryForm.controls.name.value;
    this.store.dispatch(new CreateCategoryRequested({category: newCategory}));
  }

}
