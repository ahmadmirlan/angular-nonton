import {Component, OnDestroy, OnInit} from '@angular/core';
import {select, Store} from '@ngrx/store';
import {AppState} from '../../../../core/store/reducers';
import {Category} from '../../../../core/category/model/category.model';
import {Subscription} from 'rxjs';
import {LoadAllCategoriesRequested} from '../../../../core/category/action/category.actions';
import {MatDialog} from '@angular/material/dialog';
import {CategoryDialogComponent} from '../category-dialog/category-dialog.component';
import {selectAllCategories} from '../../../../core/category/selector/category.selector';

@Component({
  selector: 'app-category-list',
  templateUrl: './category-list.component.html',
  styleUrls: ['./category-list.component.scss'],
})
export class CategoryListComponent implements OnInit, OnDestroy {
  categories: Category[] = [];
  subscriptions: Subscription[] = [];

  constructor(private store: Store<AppState>, private dialog: MatDialog) {
  }

  ngOnInit(): void {
    this.store.dispatch(new LoadAllCategoriesRequested());
    const categorySub = this.store.pipe(select(selectAllCategories)).subscribe((_categories) => {
      if (_categories) {
        this.categories = _categories;
      }
    });
    this.subscriptions.push(categorySub);
  }

  // On dialog open
  editCategoryDialog(_category?: Category): void {
    if (!_category) {
      _category = new Category();
      _category.clear();
    }
    const categoryDialog = this.dialog.open(CategoryDialogComponent, {
      width: '600px',
      maxHeight: '350px',
      minHeight: '300px',
      disableClose: true,
      panelClass: 'remove-padding-dialog'
    });
    categoryDialog.componentInstance.category = _category;
  }

  ngOnDestroy(): void {
    this.subscriptions.forEach(sub => sub.unsubscribe());
  }

}
