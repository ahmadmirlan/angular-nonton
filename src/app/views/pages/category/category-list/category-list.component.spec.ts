import {async, ComponentFixture, TestBed} from '@angular/core/testing';

import {CategoryListComponent} from './category-list.component';
import {MockStore, provideMockStore} from '@ngrx/store/testing';
import {AppState} from '../../../../core/store/reducers';
import {MemoizedSelector, Store} from '@ngrx/store';
import {CategoryState} from '../../../../core/category/reducer/category.reducer';
import {Category} from '../../../../core/category/model/category.model';
import {selectAllCategory} from '../../../../core/category/selector/category.selector';
import {By} from '@angular/platform-browser';
import {MatButtonModule} from '@angular/material/button';
import {MatTooltipModule} from '@angular/material/tooltip';
import {MatIconModule} from '@angular/material/icon';

describe('CategoryListComponent', () => {
  let component: CategoryListComponent;
  let fixture: ComponentFixture<CategoryListComponent>;
  let storeMock: MockStore<AppState>;
  let selectAllCategoryMock: MemoizedSelector<CategoryState, Category[]>;
  let categories: Category[];
  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [CategoryListComponent],
      providers: [provideMockStore()],
      imports: [
        MatButtonModule, MatTooltipModule, MatIconModule
      ]
    })
      .compileComponents();
    const cat1 = new Category();
    cat1.clear();
    cat1.id = '1';
    cat1.name = 'Action';

    const cat2 = new Category();
    cat2.clear();
    cat2.id = '1';
    cat2.name = 'Action';

    categories = [cat1, cat2];
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CategoryListComponent);
    component = fixture.componentInstance;
    storeMock = TestBed.get(Store);
    selectAllCategoryMock = storeMock.overrideSelector(selectAllCategory, categories);
    fixture.detectChanges();
  });

  it('should init categories with category value trough selectAllCategory', () => {
    selectAllCategoryMock.setResult(categories);
    storeMock.refreshState();
    fixture.detectChanges();

    expect(component.categories).toEqual(categories);
  });

  it('should render all categories data to UI', () => {
    selectAllCategoryMock.setResult(categories);
    storeMock.refreshState();
    fixture.detectChanges();

    const card = fixture.debugElement.queryAll(By.css('.category__list'));
    expect(card.length).toEqual(component.categories.length);
  });
});
