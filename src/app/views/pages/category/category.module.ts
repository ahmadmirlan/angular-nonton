import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {CategoryListComponent} from './category-list/category-list.component';
import {RouterModule, Routes} from '@angular/router';
import {MatButtonModule} from '@angular/material/button';
import {MatIconModule} from '@angular/material/icon';
import {MatTooltipModule} from '@angular/material/tooltip';
import {StoreModule} from '@ngrx/store';
import {categoryReducer} from '../../../core/category/reducer/category.reducer';
import {EffectsModule} from '@ngrx/effects';
import {CategoryEffects} from '../../../core/category/effect/category.effects';
import {CategoryDialogComponent} from './category-dialog/category-dialog.component';
import {MatDialogModule} from '@angular/material/dialog';
import {MatFormFieldModule} from '@angular/material/form-field';
import {MatInputModule} from '@angular/material/input';
import {ReactiveFormsModule} from '@angular/forms';

const ROUTES: Routes = [
  {
    path: '',
    component: CategoryListComponent
  }
];

@NgModule({
  declarations: [CategoryListComponent, CategoryDialogComponent],
  imports: [
    CommonModule,
    RouterModule.forChild(ROUTES),
    MatButtonModule,
    MatIconModule,
    MatTooltipModule,
    StoreModule.forFeature('category', categoryReducer),
    EffectsModule.forFeature([CategoryEffects]),
    MatDialogModule,
    MatFormFieldModule,
    MatInputModule,
    ReactiveFormsModule
  ],
  entryComponents: [CategoryDialogComponent]
})
export class CategoryModule {
}
