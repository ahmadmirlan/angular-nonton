export class ResponsePage {
  size: number;
  totalElements: number;
  totalPage: number;
  page: number;
  items: any[];

  clear(): void {
    this.size = 0;
    this.totalElements = 0;
    this.totalPage = 0;
    this.page = 0;
    this.items = [];
  }
}
