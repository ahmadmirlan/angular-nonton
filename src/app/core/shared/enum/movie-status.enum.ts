export enum MovieStatus {
  NOT_AVAILABLE,
  AVAILABLE,
  UPCOMING
}
