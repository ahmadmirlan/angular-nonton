import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs';
import {ResponsePage} from '../../shared/models/responsePage.model';
import {QueryParamsModel} from '../../shared/models/queryParams.model';
import {map} from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class MovieService {

  constructor(private http: HttpClient) {
  }

  // Get all published movie list
  getAllMovieList(params: QueryParamsModel): Observable<ResponsePage> {
    return this.http.post<{ data: any[], page: ResponsePage }>('server/movies/findAllPublishedMovies', params).pipe(map(resp => {
      const response = resp.page;
      response.items = resp.data;
      return response;
    }));
  }
}
