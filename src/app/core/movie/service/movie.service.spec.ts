import {TestBed} from '@angular/core/testing';

import {MovieService} from './movie.service';
import {HttpClientTestingModule, HttpTestingController} from '@angular/common/http/testing';
import {Movie} from '../model/movie.model';
import {QueryParamsModel} from '../../shared/models/queryParams.model';

describe('MovieService', () => {
  let mockHttp: HttpTestingController;
  let service: MovieService;
  let movies: Movie[];
  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule]
    });
    mockHttp = TestBed.inject(HttpTestingController);
    service = TestBed.inject(MovieService);
    const movie1 = new Movie();
    movie1.clear();
    movie1.title = 'Joker';
    movie1.id = '1';

    const movie2 = new Movie();
    movie2.clear();
    movie2.title = 'It';
    movie2.id = '2';

    movies = [movie1, movie2];
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });

  it('should get all published movie list', () => {
    const params = new QueryParamsModel({});
    service.getAllMovieList(params).subscribe((resp) => {
      expect(resp.items).toEqual(movies);
    });

    const req = mockHttp.expectOne('server/movies/findAllPublishedMovies');
    expect(req.request.method).toEqual('POST');

    const response = {
      data: movies,
      pages: {
        size: 10,
        totalElements: 5,
        totalPage: 1,
        page: 0
      }
    };
    req.flush(response);
  });
});
