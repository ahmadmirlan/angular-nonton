import {Injectable} from '@angular/core';
import {Actions, Effect, ofType} from '@ngrx/effects';
import {
  LoadAllPublishedMovieRequested,
  LoadAllPublishedMovieSuccess,
  MovieActionToggleLoading,
  MovieActionTypes,
  MoviePageToggleLoading
} from '../action/movie.actions';
import {catchError, map, mergeMap, tap} from 'rxjs/operators';
import {Store} from '@ngrx/store';
import {MovieService} from '../service/movie.service';
import {of} from 'rxjs';
import {AppState} from '../../store/reducers';


@Injectable()
export class MovieEffects {
  showPageLoadingDispatcher = new MoviePageToggleLoading({
    isLoading: true,
  });
  hidePageLoadingDispatcher = new MoviePageToggleLoading({
    isLoading: false,
  });

  showActionLoadingDispatcher = new MovieActionToggleLoading({
    isLoading: true,
  });
  hideActionLoadingDispatcher = new MovieActionToggleLoading({
    isLoading: false,
  });


  @Effect()
  loadAllPublishedMovie$ = this.actions$.pipe(
    ofType<LoadAllPublishedMovieRequested>(
      MovieActionTypes.LOAD_ALL_PUBLISHED_MOVIE_REQUESTED,
    ), mergeMap(({payload}) => {
      this.store.dispatch(this.showPageLoadingDispatcher);
      return this.movieService.getAllMovieList(payload.queryParams).pipe(tap((response) => {
          this.store.dispatch(new LoadAllPublishedMovieSuccess({response, lastQuery: payload.queryParams}));
        }), catchError(() => {
          return of(this.hidePageLoadingDispatcher);
        }),
        map(() => {
          return this.hidePageLoadingDispatcher;
        }));
    }));

  constructor(private actions$: Actions, private store: Store<AppState>, private movieService: MovieService) {
  }

}
