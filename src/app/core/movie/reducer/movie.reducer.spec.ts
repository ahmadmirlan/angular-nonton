import {initialMovieState, movieReducer} from './movie.reducer';

describe('Movie Reducer', () => {
  describe('an unknown action', () => {
    it('should return the previous state', () => {
      const action = {} as any;

      const result = movieReducer(initialMovieState, action);

      expect(result).toBe(initialMovieState);
    });
  });
});
