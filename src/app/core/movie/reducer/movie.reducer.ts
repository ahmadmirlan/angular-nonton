import {createEntityAdapter, EntityAdapter, EntityState} from '@ngrx/entity';
import {Movie} from '../model/movie.model';
import {QueryParamsModel} from '../../shared/models/queryParams.model';
import {MovieActions, MovieActionTypes} from '../action/movie.actions';

export interface MovieState extends EntityState<Movie> {
  queryRowsCount: number;
  publishedMovies: Movie[];
  queryResult: Movie[];
  listLoading: boolean;
  actionsLoading: boolean;
  lastCreatedMovieId: string;
  lastQuery: QueryParamsModel;
}

export const adapter: EntityAdapter<Movie> = createEntityAdapter<Movie>();

export const initialMovieState: MovieState = adapter.getInitialState({
  queryRowsCount: 0,
  publishedMovies: undefined,
  queryResult: undefined,
  listLoading: false,
  actionsLoading: false,
  lastCreatedMovieId: '',
  lastQuery: undefined,
});

export function movieReducer(
  state = initialMovieState,
  action: MovieActions,
): MovieState {
  switch (action.type) {
    case MovieActionTypes.MOVIE_PAGE_TOGGLE_LOADING:
      return {
        ...state,
        listLoading: action.payload.isLoading
      };
    case MovieActionTypes.MOVIE_ACTION_TOGGLE_LOADING:
      return {
        ...state,
        actionsLoading: action.payload.isLoading
      };
    case MovieActionTypes.LOAD_ALL_PUBLISHED_MOVIE_SUCCESS:
      state = adapter.addMany(action.payload.response.items, state);
      return {
        ...state,
        queryRowsCount: action.payload.response.totalElements,
        publishedMovies: action.payload.response.items,
        lastQuery: action.payload.lastQuery,
      };
    default:
      return {
        ...state
      };
  }
}

export const {
  selectAll,
  selectEntities,
  selectIds,
  selectTotal,
} = adapter.getSelectors();
