import {Action} from '@ngrx/store';
import {QueryParamsModel} from '../../shared/models/queryParams.model';
import {ResponsePage} from '../../shared/models/responsePage.model';

export enum MovieActionTypes {
  MOVIE_PAGE_TOGGLE_LOADING = '[Movie] Movie Page Toggle Loading',
  MOVIE_ACTION_TOGGLE_LOADING = '[Movie] Movie Action Toggle Loading',
  LOAD_ALL_PUBLISHED_MOVIE_REQUESTED = '[Movies API] Load All Published Movie Requested',
  LOAD_ALL_PUBLISHED_MOVIE_SUCCESS = '[Movies API] Load All Published Movie Success',
}

export class MoviePageToggleLoading implements Action {
  readonly type = MovieActionTypes.MOVIE_PAGE_TOGGLE_LOADING;

  constructor(public payload: { isLoading: boolean }) {
  }
}

export class MovieActionToggleLoading implements Action {
  readonly type = MovieActionTypes.MOVIE_ACTION_TOGGLE_LOADING;

  constructor(public payload: { isLoading: boolean }) {
  }
}

export class LoadAllPublishedMovieRequested implements Action {
  readonly type = MovieActionTypes.LOAD_ALL_PUBLISHED_MOVIE_REQUESTED;

  constructor(public payload: { queryParams: QueryParamsModel }) {
  }
}

export class LoadAllPublishedMovieSuccess implements Action {
  readonly type = MovieActionTypes.LOAD_ALL_PUBLISHED_MOVIE_SUCCESS;

  constructor(public payload: { response: ResponsePage, lastQuery: QueryParamsModel }) {
  }
}

export type MovieActions =
  MoviePageToggleLoading
  | MovieActionToggleLoading
  | LoadAllPublishedMovieRequested
  | LoadAllPublishedMovieSuccess;

