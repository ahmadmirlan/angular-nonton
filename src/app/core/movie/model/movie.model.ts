import {User} from '../../user/model/user';
import {MovieStatus} from '../../shared/enum/movie-status.enum';
import {BaseModel} from '../../shared/models/base.model';
import {Category} from '../../category/model/category.model';

export class Movie extends BaseModel {
  title: string;
  cover: string;
  actors: string[];
  releaseDate: string;
  status: MovieStatus;
  trailerURL: string;
  streamURL: string;
  description: string;
  categories: Category[];
  rentPrices: number;
  addedBy: User;

  clear(): void {
    this.id = undefined;
    this.title = '';
    this.cover = '';
    this.actors = [];
    this.releaseDate = '';
    this.status = MovieStatus.NOT_AVAILABLE;
    this.trailerURL = '';
    this.streamURL = '';
    this.description = '';
    this.categories = [];
    this.rentPrices = 0;
    this.addedBy = new User();
    this.addedBy.clear();
    this.createdAt = undefined;
    this.updatedAt = undefined;
  }
}
