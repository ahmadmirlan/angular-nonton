import {createFeatureSelector, createSelector} from '@ngrx/store';
import * as fromMovie from '../reducer/movie.reducer';
import {MovieState} from '../reducer/movie.reducer';

export const selectMovieState = createFeatureSelector<MovieState>(
  'movie',
);

export const selectMovieById = (movieId: string) =>
  createSelector(
    selectMovieState,
    movieState => movieState.entities[movieId],
  );

export const selectAllMovie = createSelector(
  selectMovieState,
  movieState => movieState.queryResult
);

export const selectAllPublishedMovie = createSelector(
  selectMovieState,
  movieState => movieState.publishedMovies
);

export const selectAllMovieId = createSelector(
  selectMovieState,
  fromMovie.selectIds,
);

export const selectMoviePageLoading = createSelector(
  selectMovieState,
  movieState => movieState.listLoading,
);

export const selectMovieActionLoading = createSelector(
  selectMovieState,
  movieState => movieState.actionsLoading,
);

export const selectLastCreatedMovieId = createSelector(
  selectMovieState,
  movieState => movieState.lastCreatedMovieId,
);
