import {AuthService} from './auth.service';
import {User} from '../../user/model/user';
import {TestBed} from '@angular/core/testing';
import {HttpClientTestingModule, HttpTestingController} from '@angular/common/http/testing';

describe('AuthService', () => {
  let httpMock: HttpTestingController;
  let mockService: AuthService;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule],
      providers: []
    });
    httpMock = TestBed.get(HttpTestingController);
    mockService = TestBed.get(AuthService);
  });

  it('should call login api', () => {
    const user = new User();
    user.clear();
    user.firstName = 'Ahmad';
    user.lastName = 'Mirlan';
    user.username = 'ahmadmirlan';
    user.email = 'ahmadmirlan64@gmail.com';

    mockService.login('ahmadmirlan', '123456').subscribe((resp) => {
      expect(resp).toEqual(user);
    });
    const req = httpMock.expectOne('server/auth/login');
    expect(req.request.method).toEqual('POST');
    req.flush({user});
  });

  it('should call register api', () => {
    const registerData = {
      firstName: 'john',
      lastName: 'doe',
      email: 'johndoe@yopmail.com',
      username: 'johndoe',
      password: '12345678'
    };

    mockService.register(registerData).subscribe(resp => {
      expect(resp).toEqual({message: 'Register success'});
    });

    const req = httpMock.expectOne('server/auth/register');
    expect(req.request.method).toEqual('POST');
    req.flush({message: 'Register success'});
  });

  it('should call api check existing email', () => {
    mockService.checkExistingEmail('jojo@yopmail.com').subscribe(resp => {
      expect(resp).toEqual(true);
    });
    const req = httpMock.expectOne('server/auth/checkEmail');
    expect(req.request.method).toEqual('POST');
    req.flush({isExist: true});
  });

  it('should call api check existing username', () => {
    mockService.checkUsername('jojojojo').subscribe(resp => {
      expect(resp).toEqual(true);
    });
    const req = httpMock.expectOne('server/auth/checkUsername');
    expect(req.request.method).toEqual('POST');
    req.flush({isExist: true});
  });
});
