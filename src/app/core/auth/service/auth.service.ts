import {Injectable} from '@angular/core';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {Observable} from 'rxjs';
import {User} from '../../user/model/user';
import {map} from 'rxjs/operators';
import {environment} from '../../../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class AuthService {

  static getLoginToken(): string {
    const userToken = localStorage.getItem(environment.authTokenKey);
    if (!userToken) {
      return '';
    }
    return userToken;
  }

  static defineHeaders(): HttpHeaders {
    return new HttpHeaders({
      Authorization: AuthService.getLoginToken(),
      'Content-Type': 'application/json',
    });
  }

  constructor(private http: HttpClient) {
  }

  // Login api
  login(username: string, password: string): Observable<User> {
    return this.http.post<{ user: User }>
    ('server/auth/login', {username, password}).pipe(map(resp => {
      const token = resp.user.accessToken;
      localStorage.setItem(environment.authTokenKey, token);
      localStorage.setItem('user', JSON.stringify(resp.user));
      return resp.user;
    }));
  }

  // Register api
  register(data: { firstName: string, lastName: string, username: string, email: string, password: string }): Observable<any> {
    return this.http.post('server/auth/register', data);
  }

  // check existing email api
  checkExistingEmail(email: string): Observable<boolean> {
    return this.http.post<{ isExist: boolean }>('server/auth/checkEmail', {email}).pipe(map(resp => {
      return resp.isExist;
    }));
  }

  // Check username
  checkUsername(username: string): Observable<boolean> {
    return this.http.post<{ isExist: boolean }>('server/auth/checkUsername', {username}).pipe(map(resp => {
      return resp.isExist;
    }));
  }

//  FInd user by token
  findUserByToken(token: string): Observable<User> {
    return this.http.get<User>('server/findUser/byToken', {headers: AuthService.defineHeaders()});
  }
}
