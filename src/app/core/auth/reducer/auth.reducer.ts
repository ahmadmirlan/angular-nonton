import {User} from '../../user/model/user';
import {AuthActions, AuthActionType} from '../action/auth.actions';


export const authFeatureKey = 'auth';

export interface AuthState {
  isLoggedIn: boolean;
  currentUser: User;
  actionLoading: boolean;
}

const initialState: AuthState = {
  isLoggedIn: false,
  currentUser: undefined,
  actionLoading: false,
};


export function authReducer(state = initialState, action: AuthActions): AuthState {
  switch (action.type) {
    case AuthActionType.IS_LOGGED_IN:
      return {
        ...state,
        isLoggedIn: action.payload.isLogin
      };
    case AuthActionType.AUTH_ACTION_LOADING:
      return {
        ...state,
        actionLoading: action.payload.isLoading
      };
    case AuthActionType.FIND_USER_BY_TOKEN_SUCCESS:
      return {
        ...state,
        currentUser: action.payload.user
      };
    default:
      return {
        ...state
      };
  }
}

export const getAuthIsLoggedIn = (state: AuthState) => state.isLoggedIn;
export const getAuthActionLoading = (state: AuthState) => state.actionLoading;
export const getAuthCurrentUser = (state: AuthState) => state.currentUser;
