import {Injectable} from '@angular/core';
import {Actions, Effect, ofType} from '@ngrx/effects';
import {defer, Observable, ObservedValueOf, of} from 'rxjs';
import {AuthActionType, FindUserByTokenRequested, FindUserByTokenSuccess, IsLoggedIn} from '../action/auth.actions';
import {environment} from '../../../../environments/environment';
import {catchError, map, switchMap} from 'rxjs/operators';
import {Store} from '@ngrx/store';
import {AppState} from '../../store/reducers';
import {AuthService} from '../service/auth.service';


@Injectable()
export class AuthEffects {
  constructor(private actions$: Actions, private store: Store<AppState>, private authService: AuthService) {
  }

  @Effect()
  findUserByToken$ = this.actions$.pipe(ofType<FindUserByTokenRequested>(AuthActionType.FIND_USER_BY_TOKEN_REQUESTED),
    map((action) => action.payload),
    switchMap(payload => {
      return this.authService
        .findUserByToken(payload.token)
        .pipe(
          map((user) => {
            this.store.dispatch(new IsLoggedIn({isLogin: true}));
            return new FindUserByTokenSuccess({user});
          }),
          catchError(error => of(new IsLoggedIn({isLogin: false})))
        );
    }));

  @Effect()
  init$: Observable<ObservedValueOf<Observable<{ type: string }>>> = defer(() => {
    const userToken = localStorage.getItem(environment.authTokenKey);
    let observableResult = of({type: 'NO_ACTION'});
    if (userToken) {
      observableResult = of(new FindUserByTokenRequested({token: userToken}));
    }
    return observableResult;
  });

}
