import {Action} from '@ngrx/store';
import {User} from '../../user/model/user';

export enum AuthActionType {
  IS_LOGGED_IN = '[Auth] Is Logged In',
  AUTH_ACTION_LOADING = '[Auth] Auth Action Loading',
  FIND_USER_BY_TOKEN_REQUESTED = '[Auth] Find User By Token Requested',
  FIND_USER_BY_TOKEN_SUCCESS = '[Auth] Find User By Token Success',
}

export class IsLoggedIn implements Action {
  readonly type = AuthActionType.IS_LOGGED_IN;

  constructor(public payload: { isLogin: boolean }) {
  }
}

export class AuthActionLoading implements Action {
  readonly type = AuthActionType.AUTH_ACTION_LOADING;

  constructor(public payload: { isLoading: boolean }) {
  }
}

export class FindUserByTokenRequested implements Action {
  readonly type = AuthActionType.FIND_USER_BY_TOKEN_REQUESTED;

  constructor(public payload: { token: string }) {
  }
}

export class FindUserByTokenSuccess implements Action {
  readonly type = AuthActionType.FIND_USER_BY_TOKEN_SUCCESS;

  constructor(public payload: { user: User }) {
  }
}

export type AuthActions = IsLoggedIn | AuthActionLoading | FindUserByTokenRequested | FindUserByTokenSuccess;
