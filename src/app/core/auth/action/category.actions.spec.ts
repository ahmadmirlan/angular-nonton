import {
  AuthActionLoading,
  AuthActionType,
  FindUserByTokenRequested,
  FindUserByTokenSuccess,
  IsLoggedIn
} from './auth.actions';
import {User} from '../../user/model/user';

describe('Auth Actions', () => {
  describe('Is Logged In', () => {
    it('should create an action', () => {
      const action = new IsLoggedIn({isLogin: true});
      expect({...action}).toEqual({
        type: AuthActionType.IS_LOGGED_IN,
        payload: {isLogin: true}
      });
    });
  });

  describe('Auth Action Loading', () => {
    it('should create a action', () => {
      const action = new AuthActionLoading({isLoading: true});
      expect({...action}).toEqual({
        type: AuthActionType.AUTH_ACTION_LOADING,
        payload: {isLoading: true}
      });
    });
  });

  describe('Find User By Token', () => {
    describe('FindUserByTokenRequested', () => {
      it('should create an action', () => {
        const action = new FindUserByTokenRequested({token: '12345Token'});
        expect({...action}).toEqual({
          type: AuthActionType.FIND_USER_BY_TOKEN_REQUESTED,
          payload: {token: '12345Token'}
        });
      });
    });

    describe('FindUserByTokenSuccess', () => {
      it('should create an action', () => {
        const user = new User();
        user.clear();
        user.username = 'johndoe';
        user.email = 'johndoe@yopmail.com';

        const action = new FindUserByTokenSuccess({user});
        expect({...action}).toEqual({
          type: AuthActionType.FIND_USER_BY_TOKEN_SUCCESS,
          payload: {user}
        });
      });
    });
  });

});
