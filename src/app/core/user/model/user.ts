export class User {
  firstName: string;
  lastName: string;
  email: string;
  username: string;
  accessToken: string;
  avatar: string;

  clear(): void {
    this.firstName = '';
    this.lastName = '';
    this.email = '';
    this.username = '';
    this.accessToken = '';
    this.avatar = '';
  }
}
