import {createFeatureSelector, createSelector} from '@ngrx/store';
import {allCategoryEntities, CategoryState} from '../reducer/category.reducer';

export const selectCategoryState = createFeatureSelector<CategoryState>(
  'category'
);

export const getAllCategoryEntities = createSelector(
  selectCategoryState,
  allCategoryEntities
);

export const selectAllCategories = createSelector(
  getAllCategoryEntities, entities => {
    if (entities) {
      return Object.keys(entities).map(categoryId => entities[categoryId]);
    }
    return undefined;
  }
);

export const selectCategoryById = (categoryId: string) => createSelector(
  selectCategoryState,
  categoryState => categoryState.entities[categoryId]
);

export const selectAllCategoryQueryResult = createSelector(
  selectCategoryState,
  categorySate => categorySate.queryResult
);
