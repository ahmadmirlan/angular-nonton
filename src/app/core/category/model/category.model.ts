import {BaseModel} from '../../shared/models/base.model';

export class Category extends BaseModel {
  name: string;

  clear(): void {
    this.id = '';
    this.name = '';
    this.createdAt = undefined;
    this.updatedAt = undefined;
  }
}
