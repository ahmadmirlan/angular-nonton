import {Injectable} from '@angular/core';
import {Actions, Effect, ofType} from '@ngrx/effects';
import {CategoryService} from '../service/category.service';
import {
  CategoryActionLoading,
  CategoryActionTypes,
  CategoryPageToggleLoading,
  CreateCategoryRequested,
  CreateCategorySuccess,
  LoadAllCategoriesRequested,
  LoadAllCategoriesSuccess
} from '../action/category.actions';
import {catchError, map, mergeMap, tap} from 'rxjs/operators';
import {AppState} from '../../store/reducers';
import {Store} from '@ngrx/store';
import {of} from 'rxjs';


@Injectable()
export class CategoryEffects {

  constructor(private actions$: Actions, private categoryService: CategoryService, private store: Store<AppState>) {
  }

  showPageLoadingDispatcher = new CategoryPageToggleLoading({
    isLoading: true,
  });
  hidePageLoadingDispatcher = new CategoryPageToggleLoading({
    isLoading: false,
  });

  showActionLoadingDispatcher = new CategoryActionLoading({
    isLoading: true,
  });
  hideActionLoadingDispatcher = new CategoryActionLoading({
    isLoading: false,
  });

  @Effect()
  loadAllCategory$ = this.actions$.pipe(ofType<LoadAllCategoriesRequested>(
    CategoryActionTypes.LOAD_ALL_CATEGORIES_REQUESTED
  ), mergeMap(() => {
    this.store.dispatch(this.showPageLoadingDispatcher);
    return this.categoryService.getAllCategories().pipe(tap((categories) => {
      this.store.dispatch(new LoadAllCategoriesSuccess({categories}));
    }), catchError(() => {
      return of(this.hidePageLoadingDispatcher);
    }), map(() => this.hidePageLoadingDispatcher));
  }));

  @Effect()
  createCategory$ = this.actions$.pipe(ofType<CreateCategoryRequested>(CategoryActionTypes.CREATE_CATEGORY_REQUESTED),
    mergeMap(({payload}) => {
      this.store.dispatch(this.showActionLoadingDispatcher);
      return this.categoryService.createNewCategory(payload.category).pipe(tap((_category) => {
          this.store.dispatch(new CreateCategorySuccess({category: _category}));
        }), catchError(() => {
          return of(this.hideActionLoadingDispatcher);
        }), map(() => this.hideActionLoadingDispatcher)
      );
    })
  );
}
