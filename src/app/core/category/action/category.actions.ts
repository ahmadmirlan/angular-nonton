import {Action} from '@ngrx/store';
import {Category} from '../model/category.model';

export enum CategoryActionTypes {
  CATEGORY_PAGE_TOGGLE_LOADING = '[Category] Category Page Toggle Loading',
  CATEGORY_ACTION_LOADING = '[Category] Category Action Loading',
  LOAD_ALL_CATEGORIES_REQUESTED = '[Category] Load All Categories Requested',
  LOAD_ALL_CATEGORIES_SUCCESS = '[Category] Load All Categories Success',
  CREATE_CATEGORY_REQUESTED = '[Category] Create Category Requested',
  CREATE_CATEGORY_SUCCESS = '[Category] Create Category Success',
}

export class CategoryPageToggleLoading implements Action {
  readonly type = CategoryActionTypes.CATEGORY_PAGE_TOGGLE_LOADING;

  constructor(public payload: { isLoading: boolean }) {
  }
}

export class CategoryActionLoading implements Action {
  readonly type = CategoryActionTypes.CATEGORY_ACTION_LOADING;

  constructor(public payload: { isLoading: boolean }) {
  }
}

export class LoadAllCategoriesRequested implements Action {
  readonly type = CategoryActionTypes.LOAD_ALL_CATEGORIES_REQUESTED;

  constructor() {
  }
}

export class LoadAllCategoriesSuccess implements Action {
  readonly type = CategoryActionTypes.LOAD_ALL_CATEGORIES_SUCCESS;

  constructor(public payload: { categories: Category[] }) {
  }
}

export class CreateCategoryRequested implements Action {
  readonly type = CategoryActionTypes.CREATE_CATEGORY_REQUESTED;

  constructor(public payload: { category: Category }) {
  }
}

export class CreateCategorySuccess implements Action {
  readonly type = CategoryActionTypes.CREATE_CATEGORY_SUCCESS;

  constructor(public payload: { category: Category }) {
  }
}

export type CategoryActions = CategoryPageToggleLoading
  | CategoryActionLoading
  | LoadAllCategoriesRequested
  | LoadAllCategoriesSuccess
  | CreateCategoryRequested
  | CreateCategorySuccess;
