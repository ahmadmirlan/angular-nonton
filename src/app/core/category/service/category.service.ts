import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs';
import {Category} from '../model/category.model';
import {AuthService} from '../../auth/service/auth.service';

@Injectable({
  providedIn: 'root'
})
export class CategoryService {

  constructor(private http: HttpClient) {
  }

  // Get all category api
  getAllCategories(): Observable<Category[]> {
    return this.http.get<Category[]>('server/categories/getAll');
  }

  // Save new category
  createNewCategory(category: Category): Observable<Category> {
    return this.http.post<Category>('server/categories/create', {name: category.name},
      {headers: AuthService.defineHeaders()});
  }
}
