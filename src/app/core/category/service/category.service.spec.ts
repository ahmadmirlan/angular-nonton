import {TestBed} from '@angular/core/testing';

import {CategoryService} from './category.service';
import {HttpClientTestingModule, HttpTestingController} from '@angular/common/http/testing';
import {Category} from '../model/category.model';

describe('CategoryService', () => {
  let service: CategoryService;
  let httpMock: HttpTestingController;
  let categories: Category[];
  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule]
    });
    service = TestBed.inject(CategoryService);
    httpMock = TestBed.inject(HttpTestingController);

    const cat1 = new Category();
    cat1.clear();
    cat1.id = '1';
    cat1.name = 'Action';

    const cat2 = new Category();
    cat2.clear();
    cat2.id = '1';
    cat2.name = 'Action';

    categories = [cat1, cat2];
  });

  it('should call api to get all categories data', () => {
    service.getAllCategories().subscribe((cat) => {
      expect(cat).toEqual(categories);
    });

    const req = httpMock.expectOne('server/categories/getAll');
    expect(req.request.method).toEqual('GET');
    req.flush(categories);
  });

  it('should create new category', () => {
    const category = new Category();
    category.clear();
    category.name = 'LoL';

    service.createNewCategory(category).subscribe((_category) => {
      expect(_category).toEqual(category);
    });

    const req = httpMock.expectOne('server/categories/create');
    expect(req.request.method).toEqual('POST');
    req.flush(category);
  });
});
