import { categoryReducer, initialCategoryState } from './category.reducer';

describe('Category Reducer', () => {
  describe('an unknown action', () => {
    it('should return the previous state', () => {
      const action = {} as any;

      const result = categoryReducer(initialCategoryState, action);

      expect(result).toBe(initialCategoryState);
    });
  });
});
