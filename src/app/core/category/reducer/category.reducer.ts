import {Category} from '../model/category.model';
import {QueryParamsModel} from '../../shared/models/queryParams.model';
import {CategoryActions, CategoryActionTypes} from '../action/category.actions';
import {createEntityAdapter, Dictionary, EntityAdapter, EntityState} from '@ngrx/entity';


export const categoryFeatureKey = 'category';

export interface CategoryState extends EntityState<Category> {
  listLoading: boolean;
  actionLoading: boolean;
  queryResult: Category[];
  lastQuery: QueryParamsModel;
  lastCreatedCategoryId: string;
}

const adapter: EntityAdapter<Category> = createEntityAdapter<Category>();

export const initialCategoryState: CategoryState = adapter.getInitialState({
    listLoading: false,
    actionLoading: false,
    queryResult: undefined,
    lastQuery: undefined,
    lastCreatedCategoryId: undefined
  }
);

export function categoryReducer(state = initialCategoryState, action: CategoryActions): CategoryState {
  switch (action.type) {

    case CategoryActionTypes.CATEGORY_PAGE_TOGGLE_LOADING:
      return {
        ...state,
        listLoading: action.payload.isLoading
      };
    case CategoryActionTypes.CATEGORY_ACTION_LOADING:
      return {
        ...state,
        actionLoading: action.payload.isLoading
      };
    case CategoryActionTypes.LOAD_ALL_CATEGORIES_SUCCESS:
      state = adapter.addMany(action.payload.categories, state);
      return {
        ...state,
        queryResult: action.payload.categories
      };
    case CategoryActionTypes.CREATE_CATEGORY_SUCCESS:
      state = adapter.addOne(action.payload.category, state);
      return {
        ...state,
        lastCreatedCategoryId: action.payload.category.id,
      };
    default:
      return state;
  }
}

export const {
  selectAll,
  selectEntities,
  selectIds,
  selectTotal,
} = adapter.getSelectors();

export const allCategoryEntities = (state: CategoryState) => state.entities;
