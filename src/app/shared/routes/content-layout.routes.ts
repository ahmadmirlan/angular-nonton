import {Routes} from '@angular/router';

// Route for content layout with sidebar, navbar and footer.
export const FULL_ROUTES: Routes = [
  {
    path: 'home',
    loadChildren: () => import('../../views/pages/home/home.module').then(m => m.HomeModule),
  },
  {
    path: 'categories',
    loadChildren: () => import('../../views/pages/category/category.module').then(m => m.CategoryModule)
  }
];
