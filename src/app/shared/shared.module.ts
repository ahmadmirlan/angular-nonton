import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { NavbarComponent } from './navbar/navbar.component';
import { ContainerComponent } from './container/container.component';
import { FooterComponent } from './footer/footer.component';
import {MatButtonModule} from '@angular/material/button';
import {_MatMenuDirectivesModule, MatMenuModule} from '@angular/material/menu';
import {RouterModule} from '@angular/router';



@NgModule({
    declarations: [NavbarComponent, ContainerComponent, FooterComponent],
  exports: [
    NavbarComponent,
    ContainerComponent,
    FooterComponent
  ],
    imports: [
        CommonModule,
        MatButtonModule,
        _MatMenuDirectivesModule,
        MatMenuModule,
        RouterModule
    ]
})
export class SharedModule { }
