import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {ContentComponent} from './layouts/content/content.component';
import {FULL_ROUTES} from './shared/routes/content-layout.routes';


const routes: Routes = [
    {
      path: 'auth',
      loadChildren: () => import('./views/pages/auth/auth.module').then(m => m.AuthModule)
    },
    {
      path: '',
      component: ContentComponent,
      children: FULL_ROUTES
    },
    {
      path: '',
      redirectTo: 'home',
      pathMatch: 'full',
    },
    {
      path: '**',
      redirectTo: 'home'
    }
  ]
;

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule {
}
